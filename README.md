## 开发

```bash
# 克隆项目
git clone https://github.com/Jinxhao18/eduSystem


# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题,看情况
npm install --registry=https://registry.npmmirror.com

# 启动服务
npm run dev
```

浏览器访问 http://localhost:80

# demo
    地址： http://www.iyycloud.com/
    账号：test001admin
    密码：admin

# 后端地址
    本地：http://124.223.188.193:40052/
    账号： admin
    密码：123456

# 之前课表提交地址，配置菜单可用来参考
https://gitee.com/daerzi/eduSystem/blob/4f115478e54e14745a2256de9fc64cff623ff327/src/router/index.js

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```