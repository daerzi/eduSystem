import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
import ParentView from '@/components/ParentView'

/**
 * Note: 路由配置项
 *
 * hidden: true                     // 当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
 * alwaysShow: true                 // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
 *                                  // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
 *                                  // 若你想不管路由下面的 children 声明的个数都显示你的根路由
 *                                  // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * redirect: noRedirect             // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * name:'router-name'               // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
 * query: '{"id": 1, "name": "ry"}' // 访问路由的默认传递参数
 * roles: ['admin', 'common']       // 访问路由的角色权限
 * permissions: ['a:a:a', 'b:b:b']  // 访问路由的菜单权限
 * meta : {
    noCache: true                   // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
    title: 'title'                  // 设置该路由在侧边栏和面包屑中展示的名字
    icon: 'svg-name'                // 设置该路由的图标，对应路径src/assets/icons/svg
    breadcrumb: false               // 如果设置为false，则不会在breadcrumb面包屑中显示
    activeMenu: '/system/user'      // 当路由设置了该属性，则会高亮相对应的侧边栏。
  }
 */

// 公共路由
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login'),
    hidden: true
  },
  // {
  //   path: '/register',
  //   component: () => import('@/views/register'),
  //   hidden: true
  // },
  {
    path: '/404',
    component: () => import('@/views/error/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error/401'),
    hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: '/teachingTask/studentInfo',
    hidden: true,
    children: [
      {
        path: 'index',
        redirect: '/teachingTask/studentInfo',
        component: () => import('@/views/index'),
        name: 'Index',
        meta: { title: '首页', icon: 'dashboard', affix: true }
      }
    ]
  },
  {
    path: '/user',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [
      {
        path: 'profile',
        component: () => import('@/views/system/user/profile/index'),
        name: 'Profile',
        meta: { title: '个人中心', icon: 'user' }
      }
    ]
  },
  {
    path: '/school',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [
      {
        path: 'timetable',
        component: () => import('@/views/timeTable/teacher/schedule'),
        name: '1',
        meta: { title: '课表', icon: 'user' }
      },
      {
        path: 'timetableImportIndex',
        component: () => import('@/views/timeTable/import/index'),
        name: '2',
        meta: { title: '课表导入', icon: 'user' }
      },
      {
        path: 'timetableImportList',
        component: () => import('@/views/timeTable/import/timetableImportList'),
        name: '3',
        meta: { title: '课表导入', icon: 'user' }
      },
      {
        path: 'timetableImport',
        component: () => import('@/views/timeTable/import/timetableImport'),
        name: '4',
        meta: { title: '课表导入', icon: 'user' }
      },
    ]
  },
  {
    path: '/detail',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [
      {
        path: '1',
        component: () => import('@/views/attendanceManagement/rollCall/detail'),
        name: '1',
        meta: { title: '详情', icon: 'user' }
      }
    ]
  },
  {
    path: '/class',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [
      {
        path: 'teachingStudent',
        component: () => import('@/views/class/teachingStudent'),
        name: '1',
        meta: { title: '课表', icon: 'user' }
      },
      {
        path: 'student',
        component: () => import('@/views/class/student'),
        name: '1',
        meta: { title: '课表', icon: 'user' }
      }
    ]
  },
]

// 动态路由，基于用户权限动态去加载
export const dynamicRoutes = [
  {
    path: '/schedule/adjust/detail',
    component: Layout,
    hidden: true,
    children: [
      {
        path: 'set',
        component: () => import('@/views/schedule/adjust/detail'),
        name: 'setting',
        meta: { title: '调课管理', activeMenu: '/schedule/adjust' }
      }
    ]
  },
  // {
  //   name: "TeacherManage",
  //   path: "/teachingTask",
  //   component: Layout,
  //   hidden: false,
  //   redirect: "noRedirect",
  //   alwaysShow: true,
  //   meta: { title: '课程任务管理', icon: 'documentation',link:null },
  //   children: [
  // {
  //   path: '/teachingTask/studentInfo',
  //   component: () => import('@/views/teachingTask/teacherManage/teacherInfo'),
  //   name: 'TeacherInfo',
  //   hidden: false,
  //   // redirect: "noRedirect",
  //   meta: { title: '课程任务学生', icon: 'documentation'},
  // },
  // {
  //   path: '/teachingTask/teacherInfo',
  //   component: () => import('@/views/teachingTask/teacherManage/teacherInfo'),
  //   name: 'TeacherInfo',
  //   hidden: false,
  //   // redirect: "noRedirect",
  //   meta: { title: '课程任务教师', icon: 'documentation'},
  // },
  // {
  //   name: "TeacherManage",
  //   path: "/teachingTask/teacherManage",
  //   component: () => import('@/views/teachingTask/teacherManage/teacherInfo'),
  //   hidden: false,
  //   redirect: "noRedirect",
  //   alwaysShow: true,
  //   meta: { title: '学籍异动管理', icon: 'documentation',link:null },
  // },
  // {
  //   name: "TeachingTask",
  //   path: "/teachingTask/teachingTask",
  //   component:  () => import('@/views/teachingTask/teachingTask/index'),
  //   hidden: false,
  //   meta: { title: '教学任务管理', icon: 'documentation',link:null },
  // },
  // {
  //   name: "TeacherClassHour",
  //   path: "/teachingTask/classHour",
  //   component: () => import('@/views/teachingTask/teacherClassHour/index'),
  //   meta: { title: '教师课时量统计', icon: 'documentation',link:null },
  // },
  // {
  //   name: "studentInfo",
  //   path: "/teachingTask/studentManage/studentInfo",
  //   component: () => import('@/views/teachingTask/studentManage/studentInfo'),
  //   meta: { title: '学生个人课表', icon: 'documentation',link:null },
  // },
  // {
  //   name: "studentInfo",
  //   path: "/teachingTask/studentManage/studentInfo",
  //   component: () => import('@/views/teachingTask/studentManage/studentInfo'),
  //   meta: { title: '教师个人课表', icon: 'documentation',link:null },
  // },
  // {
  //   name: "studentInfo",
  //   path: "/teachingTask/studentManage/studentInfo",
  //   component: () => import('@/views/teachingTask/studentManage/studentInfo'),
  //   meta: { title: '课程任务数据获取', icon: 'documentation',link:null },
  // }
  //   ]
  // },

  // {
  //   name: "classroomManage",
  //   path: "/classroom",
  //   hidden: false,
  //   redirect: "noRedirect",
  //   component: Layout,
  //   alwaysShow: true,
  //   permissions: ['admin'],
  //   meta: { title: '教室管理', icon: 'documentation'},
  //   children: [
  //     {
  //       name: "ClassroomList",
  //       path: "/classroom/list",
  //       component: () => import('@/views/classroom/list'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '教室信息', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "AddClassroom",
  //       path: "/classroom/add",
  //       component: () => import('@/views/classroom/add'),
  //       hidden: true,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '教室信息', icon: 'documentation',link:null },
  //     }
  //   ],
  // },
  // {
  //   name: "Course",
  //   path: "/Course",
  //   redirect: "noRedirect",
  //   component: Layout,
  //   permissions: ['admin'],
  //   alwaysShow: true,

  //   meta: { title: '课程管理', icon: 'documentation',link:null },

  //   children: [
  //     {
  //       name: "Course",
  //       path: "/course/list",
  //       component: () => import('@/views/course/index'),
  //       permissions: ['admin'],
  //       meta: { title: '课程信息', icon: 'documentation',link:null },
  //     },
  //   ],
  // },
  // {
  //   name: "classroomManage",
  //   path: "/classroom",
  //   hidden: false,
  //   redirect: "noRedirect",
  //   component: Layout,
  //   alwaysShow: true,
  //   permissions: ['admin'],
  //   meta: { title: '走班管理', icon: 'documentation'},
  //   children: [
  //     {
  //       name: "ClassroomList",
  //       path: "/classroom/list",
  //       component: () => import('@/views/classroom/list'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '行政班管理', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "AddClassroom",
  //       path: "/classroom/add",
  //       component: () => import('@/views/classroom/add'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '教学班管理', icon: 'documentation',link:null },
  //     }
  //   ],
  // },
  // {
  //   name: "classroomManage",
  //   path: "/classroom",
  //   hidden: false,
  //   redirect: "noRedirect",
  //   component: Layout,
  //   alwaysShow: true,
  //   permissions: ['admin'],
  //   meta: { title: '选课管理', icon: 'documentation'},
  //   children: [
  //     {
  //       name: "ClassroomList",
  //       path: "/classroom/list",
  //       component: () => import('@/views/classroom/list'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '定义模式', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "AddClassroom",
  //       path: "/classroom/add",
  //       component: () => import('@/views/classroom/add'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '选课设置发布', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "AddClassroom",
  //       path: "/classroom/add",
  //       component: () => import('@/views/classroom/add'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '选课信息管理', icon: 'documentation',link:null },
  //     },

  //     {
  //       name: "AddClassroom",
  //       path: "/classroom/add",
  //       component: () => import('@/views/classroom/add'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '选课监控统计', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "AddClassroom",
  //       path: "/classroom/add",
  //       component: () => import('@/views/classroom/add'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '结果修改', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "AddClassroom",
  //       path: "/classroom/add",
  //       component: () => import('@/views/classroom/add'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '学生选课', icon: 'documentation',link:null },
  //     },
  //   ],
  // },
  // {
  //   name: "classroomManage",
  //   path: "/classroom",
  //   hidden: false,
  //   redirect: "noRedirect",
  //   component: Layout,
  //   alwaysShow: true,
  //   permissions: ['admin'],
  //   meta: { title: '排课管理', icon: 'documentation'},
  //   children: [
  //     {
  //       name: "ClassroomList",
  //       path: "/classroom/list",
  //       component: () => import('@/views/classroom/list'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '课时节次管理', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "AddClassroom",
  //       path: "/classroom/add",
  //       component: () => import('@/views/classroom/add'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '分班管理', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "AddClassroom",
  //       path: "/classroom/add",
  //       component: () => import('@/views/classroom/add'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '排课流程管理', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "AddClassroom",
  //       path: "/classroom/add",
  //       component: () => import('@/views/classroom/add'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '排课有效性检查', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "AddClassroom",
  //       path: "/classroom/add",
  //       component: () => import('@/views/classroom/add'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '调课管理', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "AddClassroom",
  //       path: "/classroom/add",
  //       component: () => import('@/views/classroom/add'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '排课统计', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "AddClassroom",
  //       path: "/classroom/add",
  //       component: () => import('@/views/classroom/add'),
  //       hidden: true,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '以往课表录入', icon: 'documentation',link:null },
  //     }
  //   ],
  // },
  // {
  //   name: "classroomManage",
  //   path: "/classroom",
  //   hidden: false,
  //   redirect: "noRedirect",
  //   component: Layout,
  //   alwaysShow: true,
  //   permissions: ['admin'],
  //   meta: { title: '课表管理', icon: 'documentation'},
  //   children: [
  //     {
  //       name: "ClassroomList",
  //       path: "/classroom/list",
  //       component: () => import('@/views/classroom/list'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '学生课表管理', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "AddClassroom",
  //       path: "/classroom/add",
  //       component: () => import('@/views/classroom/add'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '教师课表管理', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "AddClassroom",
  //       path: "/classroom/add",
  //       component: () => import('@/views/classroom/add'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '行政班课表管理', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "AddClassroom",
  //       path: "/classroom/add",
  //       component: () => import('@/views/classroom/add'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '教学班课表管理', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "AddClassroom",
  //       path: "/classroom/add",
  //       component: () => import('@/views/classroom/add'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '年级课表', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "AddClassroom",
  //       path: "/classroom/add",
  //       component: () => import('@/views/classroom/add'),
  //       hidden: false,
  //       // redirect: "noRedirect",
  //       // alwaysShow: true,
  //       meta: { title: '教室课表', icon: 'documentation',link:null },
  //     }
  //   ],
  // },
  // {
  //   name: "semesterManage",
  //   path: "",
  //   redirect: "/semester",
  //   component: Layout,
  //   hidden: true,
  //   permissions: ['admin'],
  //   meta: { title: '学年学期管理', icon: 'documentation'},
  //   children: [
  //     {
  //       name: "Semester",
  //       path: "/semester",
  //       component: () => import('@/views/semester/index'),
  //       permissions: ['admin'],
  //       meta: { title: '显示偏好设置', icon: 'documentation',link:null },
  //     },
  //   ],
  // },
  // {
  //   name: "gradesManage",
  //   path: "",
  //   // hidden: false,
  //   redirect: "/grades",
  //   component: Layout,
  //   // alwaysShow: true,
  //   permissions: ['admin'],
  //   meta: { title: '分班成绩管理', icon: 'documentation',link:null },

  //   children: [
  //     {
  //       name: "Grades",
  //       path: "/grades",
  //       component: () => import('@/views/grades/index'),
  //       permissions: ['admin'],
  //       meta: { title: '显示偏好设置', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "Grades",
  //       path: "/grades",
  //       component: () => import('@/views/grades/index'),
  //       permissions: ['admin'],
  //       meta: { title: '学生成绩', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "Grades",
  //       path: "/grades",
  //       component: () => import('@/views/grades/index'),
  //       permissions: ['admin'],
  //       meta: { title: '班级教学分析—行政班', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "Grades",
  //       path: "/grades",
  //       component: () => import('@/views/grades/index'),
  //       permissions: ['admin'],
  //       meta: { title: '班级教学分析—行政班', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "Grades",
  //       path: "/grades",
  //       component: () => import('@/views/grades/index'),
  //       permissions: ['admin'],
  //       meta: { title: '班级教学分析—教学班', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "Grades",
  //       path: "/grades",
  //       component: () => import('@/views/grades/index'),
  //       permissions: ['admin'],
  //       meta: { title: '学生成绩单', icon: 'documentation',link:null },
  //     },
  //   ],
  // },
  // {
  //   name: "authManage",
  //   path: "",
  //   redirect: "/auth",
  //   component: Layout,
  //   permissions: ['admin'],
  //   children: [
  //     {
  //       name: "Auth",
  //       path: "/auth",
  //       component: () => import('@/views/auth/index'),
  //       permissions: ['admin'],
  //       meta: { title: '身份授权', icon: 'documentation',link:null },
  //     },
  //   ],
  // },
  // {
  //   name: "dictManage",
  //   path: "",
  //   redirect: "/dict",
  //   component: Layout,
  //   permissions: ['admin'],
  //   meta: { title: '字典管理', icon: 'documentation'},
  //   children: [
  //     {
  //       name: "Dict",
  //       path: "/dict",
  //       component: () => import('@/views/dict/index'),
  //       permissions: ['admin'],
  //       meta: { title: '学年学期管理', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "Dict",
  //       path: "/dict",
  //       component: () => import('@/views/dict/index'),
  //       permissions: ['admin'],
  //       meta: { title: '全局字典定义', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "Dict",
  //       path: "/dict",
  //       component: () => import('@/views/dict/index'),
  //       permissions: ['admin'],
  //       meta: { title: '用户字典定义', icon: 'documentation',link:null },
  //     },
  //   ],
  // },
  // {
  //   name: "logManage",
  //   path: "",
  //   redirect: "/log",
  //   component: Layout,
  //   permissions: ['admin'],
  //   meta: { title: '日志管理', icon: 'documentation' },
  //   children: [
  //     {
  //       name: "Log",
  //       path: "/log",
  //       component: () => import('@/views/log/index'),
  //       permissions: ['admin'],
  //       meta: { title: '系统运行日志', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "Log",
  //       path: "/log",
  //       component: () => import('@/views/log/index'),
  //       permissions: ['admin'],
  //       meta: { title: '系统运行日志', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "Log",
  //       path: "/log",
  //       component: () => import('@/views/log/index'),
  //       permissions: ['admin'],
  //       meta: { title: '用户操作日志', icon: 'documentation',link:null },
  //     },
  //     {
  //       name: "Log",
  //       path: "/log",
  //       component: () => import('@/views/log/index'),
  //       permissions: ['admin'],
  //       meta: { title: '访问熔断管理', icon: 'documentation',link:null },
  //     },
  //   ],
  // },
  {
    path: '/system/user-auth',
    component: Layout,
    hidden: true,
    permissions: ['system:user:edit'],
    children: [
      {
        path: 'role/:userId(\\d+)',
        component: () => import('@/views/system/user/authRole'),
        name: 'AuthRole',
        meta: { title: '分配角色', activeMenu: '/system/user' }
      }
    ]
  },
  {
    path: '/system/role-auth',
    component: Layout,
    hidden: true,
    permissions: ['system:role:edit'],
    children: [
      {
        path: 'user/:roleId(\\d+)',
        component: () => import('@/views/system/role/authUser'),
        name: 'AuthUser',
        meta: { title: '分配用户', activeMenu: '/system/role' }
      }
    ]
  },
  {
    path: '/system/academicYearSemesterManagement-data',
    component: Layout,
    hidden: true,
    permissions: ['system:dict:list'],
    children: [
      {
        path: 'index/:dictId(\\d+)',
        component: () => import('@/views/system/dict/data'),
        name: 'Data',
        meta: { title: '字典数据', activeMenu: '/dictManage/academicYearSemesterManagement' }
      }
    ]
  },
  {
    path: '/system/globalDictionaryDefinition-data',
    component: Layout,
    hidden: true,
    permissions: ['system:dict:list'],
    children: [
      {
        path: 'index/:dictId(\\d+)',
        component: () => import('@/views/system/dict/data'),
        name: 'Data',
        meta: { title: '字典数据', activeMenu: '/dictManage/globalDictionaryDefinition' }
      }
    ]
  },
  {
    path: '/system/userDictionaryDefinition-data',
    component: Layout,
    hidden: true,
    permissions: ['system:dict:list'],
    children: [
      {
        path: 'index/:dictId(\\d+)',
        component: () => import('@/views/system/dict/data'),
        name: 'Data',
        meta: { title: '字典数据', activeMenu: '/dictManage/userDictionaryDefinition' }
      }
    ]
  },
  // {
  //   path: '/monitor/job-log',
  //   component: Layout,
  //   hidden: true,
  //   permissions: ['monitor:job:list'],
  //   children: [
  //     {
  //       path: 'index/:jobId(\\d+)',
  //       component: () => import('@/views/monitor/job/log'),
  //       name: 'JobLog',
  //       meta: { title: '调度日志', activeMenu: '/monitor/job' }
  //     }
  //   ]
  // },
  // {
  //   path: '/tool/gen-edit',
  //   component: Layout,
  //   hidden: true,
  //   permissions: ['tool:gen:edit'],
  //   children: [
  //     {
  //       path: 'index/:tableId(\\d+)',
  //       component: () => import('@/views/tool/gen/editTable'),
  //       name: 'GenEdit',
  //       meta: { title: '修改生成配置', activeMenu: '/tool/gen' }
  //     }
  //   ]
  // }
]

// 防止连续点击多次路由报错
let routerPush = Router.prototype.push;
let routerReplace = Router.prototype.replace;
// push
Router.prototype.push = function push (location) {
  return routerPush.call(this, location).catch(err => err)
}
// replace
Router.prototype.replace = function push (location) {
  return routerReplace.call(this, location).catch(err => err)
}

export default new Router({
  mode: 'history', // 去掉url中的#
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})
