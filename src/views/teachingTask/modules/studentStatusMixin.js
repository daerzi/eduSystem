import request  from '@/utils/request';

export default {
  data(){
    return {
      title:"操作",
      visible: false,
      form: {},
      disableSubmit: false,
      confirmLoading:false,
      rules: {},
      url: {
        add: '/system/studentstatus',
        edit: '',
      },
    }
  },
  methods:{
    deal(title, STATUS, record, search){
      //console.log('edit record:', JSON.parse(JSON.stringify(record)));
      this.title = title + '信息录入';
      this.form.schoolYear = search.schoolYear;
      this.form.term = search.term;
      this.form.STUDENT = record.number;
      this.form.STATUS = STATUS;
      this.visible = true;
    },
    add () {
      this.visible = true;
    },
    edit (record) {
      console.log('edit record:', record);
      //this.form = Object.assign({}, record);
      this.form.id = record.id;
      this.visible = true;
    },
    close () {
      this.$refs.form.resetFields();
      this.$emit('close');
      this.visible = false;
    },
    handleCancel () {
      this.close()
    },
    handleOk () {
      const that = this;
      this.$refs.form.validate((valid) => {
        if (valid) {
          that.confirmLoading = true;
          let url = this.form.id? this.url.edit : this.url.add;
          request({ url, method: 'post', data:this.form}).then((res)=>{
            if(res.code==200){
              that.$message.success(res.msg);
              that.$emit('ok');
              that.close();
            }else{
              that.$message.error(res.msg);
            }
          }).catch((error)=>{
            that.$message.error(error);
          }).finally(() => {
            that.confirmLoading = false;
          })
        } else {
          console.log('error submit!!');
          return false;
        }
      });
    },
  }
}