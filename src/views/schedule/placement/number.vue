<template>
  <div class="app-container">
    <el-table v-loading="loading" :data="tableData">
      <el-table-column label="科目" align="center" prop="name" />
      <el-table-column
        label="选课人数"
        align="center"
        prop="studentcount"
        :show-overflow-tooltip="true"
      >
        <template slot-scope="scope">
          {{
            scope.row.studentcountA * 1 +
            scope.row.studentcountB * 1 +
            scope.row.studentcountC * 1
          }}
        </template>
      </el-table-column>
      <el-table-column label="A级人数" align="center">
        <template slot-scope="scope">
          <el-input
            v-model="scope.row.studentcountA"
            type="number"
            @blur="handleChangeStudentcount(scope.row)"
            :max="scope.row.studentcount - scope.row.studentcountB"
            min="0"
          ></el-input>
        </template>
      </el-table-column>
      <el-table-column label="班级数量" align="center">
        <template slot-scope="scope">
          <el-input v-model="scope.row.classcountA" min="0"></el-input>
        </template>
      </el-table-column>
      <el-table-column
        label="每班人数"
        align="center"
        prop="classcountA"
        :show-overflow-tooltip="true"
      >
        <template slot-scope="scope">
          {{
            scope.row.classcountA
              ? ((scope.row.studentcountA * 1) / scope.row.classcountA) * 1
              : 0
          }}
        </template>
      </el-table-column>
      <el-table-column label="B级人数" align="center">
        <template slot-scope="scope">
          <el-input
            v-model="scope.row.studentcountB"
            type="number"
            @blur="handleChangeStudentcount(scope.row)"
            :max="scope.row.studentcount - scope.row.studentcountA"
            min="0"
          ></el-input>
        </template>
      </el-table-column>
      <el-table-column label="班级数量" align="center">
        <template slot-scope="scope">
          <el-input v-model="scope.row.classcountB" min="0"></el-input>
        </template>
      </el-table-column>
      <el-table-column
        label="每班人数"
        align="center"
        prop="classcountB"
        :show-overflow-tooltip="true"
      >
        <template slot-scope="scope">
          {{
            scope.row.classcountB
              ? ((scope.row.studentcountB * 1) / scope.row.classcountB) * 1
              : 0
          }}
        </template>
      </el-table-column>
      <el-table-column label="C级人数" align="center">
        <template slot-scope="scope">
          <el-input disabled v-model="scope.row.studentcountC"></el-input>
        </template>
      </el-table-column>
      <el-table-column label="班级数量" align="center">
        <template slot-scope="scope">
          <el-input v-model="scope.row.classcountC"></el-input>
        </template>
      </el-table-column>
      <el-table-column
        label="每班人数"
        prop="roleName"
        :show-overflow-tooltip="true"
      >
        <template slot-scope="scope">
          {{
            scope.row.classcountC
              ? ((scope.row.studentcountC * 1) / scope.row.classcountC) * 1
              : 0
          }}
        </template>
      </el-table-column>
    </el-table>
    <div class="flex-justify-center mt20 mb20">
      <el-form :inline="true" :model="formData" class="search-form">
        <el-form-item label="期望的各班平均分差" class="grads">
          <el-input v-model="formData.avg"></el-input>
        </el-form-item>
        <el-button type="primary" @click="handleSubmit">确认</el-button>
      </el-form>
    </div>
    <div class="summary">
      一共<span class="color-active">{{ classNum }}</span
      >个班，<span class="color-active">{{ studentNum }}</span
      >名同学， 平均每班<span class="color-active">{{
        classNum ? studentNum / classNum : 0
      }}</span
      >人，参与选课<span class="color-active">{{ studentCount }}</span
      >人，平均每班<span class="color-active">{{
        classNum ? studentCount / classNum : 0
      }}</span
      >人
    </div>
  </div>
</template>

<script>
import Steps from '../component/steps/index.vue'
import { getDivideList } from "@/api/schedule/number";

export default {
  name: "SchedulePlacementNumber",
  dicts: ['sys_normal_disable'],
  components: {
    Steps
  },
  props: {
    params: {
      type: Object,
      default: null
    }
  },
  data () {
    return {
      // 遮罩层
      loading: true,
      // 总条数
      total: 0,
      // 角色表格数据
      tableData: [],
      // 是否显示弹出层
      open: false,
      formData: {
        avg: '0.0'//期望的各班平均分差
      },
      classNum: 0,
      studentNum: 0,
      studentCount: 0,
    };
  },
  computed: {
  },
  created () {
    this.getList();
  },
  methods: {
    /** 查询角色列表 */
    getList () {
      this.loading = true;
      getDivideList(this.params).then(res => {
        this.loading = false;
        let studentnumKey = Object.keys(res.rows[0]).indexOf('studentnum')
        console.log(studentnumKey)
        Object.values(res.rows[0]).forEach((item, index) => {
          if (studentnumKey !== index) {
            this.tableData.push(item)

          }
        })
        const studentnum = res.rows[0].studentnum
        this.studentNum = studentnum.studentcount
        this.studentCount = this.studentNum - studentnum.section
        this.classNum += studentnum.classcountA + studentnum.classcountB + studentnum.classcountC
      }).catch(err => {
        this.loading = false;

      });;
    },
    handleChangeStudentcount (row) {
      row.studentcountC = row.studentcount - row.studentcountA - row.studentcountB
    },
    // handleChangeClasscount (row) {
    //   row.classcountC = row.classcount - row.studentcountA - row.studentcountB
    // },
    /** 提交按钮 */
    handleSubmit () {
      let params = {
        ...this.params,
        names: this.tableData,
        avg: this.formData.avg
      }
      this.$emit('handleNext', params)
    },
  }
};
</script>
<style scoped lang="scss">
.summary {
  background: #f4f8f9;
  padding: 10px 0;
  text-align: center;
}
</style>