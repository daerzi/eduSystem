import request from '@/utils/request'

// 获取路由
export const getRouters = () => {
  return request({
    url: '/getRouters',
    method: 'get'
  })
//   return new Promise((resolve, reject) => {
//     const data = [
//       {
//           "name": "TeachingTask",
//           "path": "/teachingTask",
//           "hidden": false,
//           "redirect": "noRedirect",
//           "component": "Layout",
//           "alwaysShow": true,
//           "meta": {
//               "title": "课程任务管理",
//               "icon": "education",
//               "noCache": false,
//               "link": null
//           },
//           "children": [
//               {
//                   "name": "StudentInfo",
//                   "path": "studentInfo",
//                   "hidden": false,
//                   "component": "teachingTask/teacherManage/teacherInfo",
//                   "meta": {
//                       "title": "课程任务学生",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "TeacherInfo",
//                   "path": "teacherInfo",
//                   "hidden": false,
//                   "component": "teachingTask/teacherManage/teacherInfo",
//                   "meta": {
//                       "title": "课程任务教师",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "StudentStatusChange",
//                   "path": "studentStatusChange",
//                   "hidden": false,
//                   "component": "teachingTask/teacherManage/teacherInfo",
//                   "meta": {
//                       "title": "学籍异动管理",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "TeachingTask",
//                   "path": "teachingTask",
//                   "hidden": false,
//                   "component": "teachingTask/teachingTask/index",
//                   "meta": {
//                       "title": "教学任务管理",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "TeacherClassHour",
//                   "path": "teacherClassHour",
//                   "hidden": false,
//                   "component": "teachingTask/teacherClassHour/index",
//                   "meta": {
//                       "title": "教师课时量统计",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": " studentSchedule",
//                   "path": " studentSchedule",
//                   "hidden": false,
//                   "component": "teachingTask/studentManage/studentInfo",
//                   "meta": {
//                       "title": "学生个人课表",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "TeacherSchedule",
//                   "path": "teacherSchedule",
//                   "hidden": false,
//                   "component": "teachingTask/studentManage/studentInfo",
//                   "meta": {
//                       "title": "教师个人课表",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "CourseTask",
//                   "path": "courseTask",
//                   "hidden": false,
//                   "component": "teachingTask/studentManage/studentInfo",
//                   "meta": {
//                       "title": "课程任务数据获取",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               }
//           ]
//       },
//       {
//           "name": "/classroom",
//           "path": "//classroom",
//           "hidden": false,
//           "redirect": "noRedirect",
//           "component": "Layout",
//           "alwaysShow": true,
//           "meta": {
//               "title": "教室管理",
//               "icon": "table",
//               "noCache": false,
//               "link": null
//           },
//           "children": [
//               {
//                   "name": "/classrrom/list",
//                   "path": "/classrrom/list",
//                   "hidden": false,
//                   "component": "classroom/list",
//                   "meta": {
//                       "title": "教室信息",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               }
//           ]
//       },
//       {
//           "name": "Course",
//           "path": "/course",
//           "hidden": false,
//           "redirect": "noRedirect",
//           "component": "Layout",
//           "alwaysShow": true,
//           "meta": {
//               "title": "课程管理",
//               "icon": "documentation",
//               "noCache": false,
//               "link": null
//           },
//           "children": [
//               {
//                   "name": "List",
//                   "path": "list",
//                   "hidden": false,
//                   "component": "course/index",
//                   "meta": {
//                       "title": "课程信息",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               }
//           ]
//       },
//       {
//           "name": "ShiftManagement",
//           "path": "/class",
//           "hidden": false,
//           "redirect": "noRedirect",
//           "component": "Layout",
//           "alwaysShow": true,
//           "meta": {
//               "title": "走班管理",
//               "icon": "tab",
//               "noCache": false,
//               "link": null
//           },
//           "children": [
//               {
//                   "name": "ClassManage",
//                   "path": "/class/manage",
//                   "hidden": false,
//                   "component": "class/manage",
//                   "meta": {
//                       "title": "行政班管理",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                 "name": "ClassManageStudent",
//                 "path": "/class/manage/student/:id",
//                 "hidden": true,
//                 "component": "class/student",
//                 "meta": {
//                     "title": "行政班管理学生",
//                     "icon": "#",
//                     "noCache": false,
//                     "link": null,
//                 }
//             },
//               {
//                   "name": "ClassTeaching",
//                   "path": "/class/teaching",
//                   "hidden": false,
//                   "component": "class/teaching",
//                   "meta": {
//                       "title": "教学班管理",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                 "name": "ClassTeachingStudent",
//                 "path": "/class/teaching/student",
//                 "hidden": true,
//                 "component": "class/teachingStudent",
//                 "meta": {
//                     "title": "行政班管理学生",
//                     "icon": "#",
//                     "noCache": false,
//                     "link": null,
//                 },
//             },
//                 {
//                     "name": "ClassSchedule",
//                     "path": "/class/schedule/:id",
//                     "hidden": true,
//                     "component": "class/schedule",
//                     "meta": {
//                         "title": "课表",
//                         "icon": "#",
//                         "noCache": false,
//                         "link": null,
//                     }
//                 }
//             ]
//       },
//       {
//           "name": "CourseSelectionManagement",
//           "path": "/courseSelectionManagement",
//           "hidden": false,
//           "redirect": "noRedirect",
//           "component": "Layout",
//           "alwaysShow": true,
//           "meta": {
//               "title": "选课管理",
//               "icon": "documentation",
//               "noCache": false,
//               "link": null
//           },
//           "children": [
//               {
//                   "name": "DefineMode",
//                   "path": "defineMode",
//                   "hidden": false,
//                   "component": "classroom/list",
//                   "meta": {
//                       "title": "定义模式",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "CourseSelectionSettingsPublishing",
//                   "path": "courseSelectionSettingsPublishing",
//                   "hidden": false,
//                   "component": "classroom/list",
//                   "meta": {
//                       "title": "选课设置发布",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "CourseSelectionInfoManagement",
//                   "path": "courseSelectionInfoManagement",
//                   "hidden": false,
//                   "component": "classroom/list",
//                   "meta": {
//                       "title": "选课信息管理",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "CourseSelectionMonitoringStatistics",
//                   "path": "courseSelectionMonitoringStatistics",
//                   "hidden": false,
//                   "component": "classroom/list",
//                   "meta": {
//                       "title": "选课监控统计",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "ResultModification",
//                   "path": "resultModification",
//                   "hidden": false,
//                   "component": "classroom/list",
//                   "meta": {
//                       "title": "结果修改",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "StudentCourseSelection",
//                   "path": "studentCourseSelection",
//                   "hidden": false,
//                   "component": "classroom/list",
//                   "meta": {
//                       "title": "学生选课",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               }
//           ]
//       },
//       {
//           "name": "/schedule",
//           "path": "//schedule",
//           "hidden": false,
//           "redirect": "noRedirect",
//           "component": "Layout",
//           "alwaysShow": true,
//           "meta": {
//               "title": "排课管理",
//               "icon": "tree-table",
//               "noCache": false,
//               "link": null
//           },
//           "children": [
//               {
//                   "name": "/schedule/session",
//                   "path": "/schedule/session",
//                   "hidden": false,
//                   "component": "schedule/session/index",
//                   "meta": {
//                       "title": "课时节次管理",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "/schedule/placement",
//                   "path": "/schedule/placement",
//                   "hidden": false,
//                   "component": "schedule/placement/index",
//                   "meta": {
//                       "title": "分班管理",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "/schedule/process",
//                   "path": "/schedule/process",
//                   "hidden": false,
//                   "component": "schedule/process/index",
//                   "meta": {
//                       "title": "排课流程管理",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "/schedule/validation",
//                   "path": "/schedule/validation",
//                   "hidden": false,
//                   "component": "schedule/validation/index",
//                   "meta": {
//                       "title": "排课有效性检查",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "/schedule/adjust",
//                   "path": "/schedule/adjust",
//                   "hidden": false,
//                   "component": "schedule/adjust/index",
//                   "meta": {
//                       "title": "调课管理",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "/schedule/statistics",
//                   "path": "/schedule/statistics",
//                   "hidden": false,
//                   "component": "schedule/statistics/index",
//                   "meta": {
//                       "title": "排课统计",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "PreviousScheduleEntry",
//                   "path": "previousScheduleEntry",
//                   "hidden": true,
//                   "component": "classroom/list",
//                   "meta": {
//                       "title": "以往课表录入",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               }
//           ]
//       },
//       {
//           "name": "ScheduleManagement",
//           "path": "/scheduleManagement",
//           "hidden": false,
//           "redirect": "noRedirect",
//           "component": "Layout",
//           "alwaysShow": true,
//           "meta": {
//               "title": "课表管理",
//               "icon": "zip",
//               "noCache": false,
//               "link": null
//           },
//           "children": [
//               {
//                   "name": "StudentScheduleManagement",
//                   "path": "studentScheduleManagement",
//                   "hidden": false,
//                   "component": "classroom/list",
//                   "meta": {
//                       "title": "学生课表管理",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "TeacherScheduleManagement",
//                   "path": "teacherScheduleManagement",
//                   "hidden": false,
//                   "component": "classroom/list",
//                   "meta": {
//                       "title": "教师课表管理",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "AdministrationClassScheduleManagement",
//                   "path": "administrationClassScheduleManagement",
//                   "hidden": false,
//                   "component": "classroom/list",
//                   "meta": {
//                       "title": "行政班课表管理",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "TeachingClassScheduleManagement",
//                   "path": "teachingClassScheduleManagement",
//                   "hidden": false,
//                   "component": "classroom/list",
//                   "meta": {
//                       "title": "教学班课表管理",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "GradeSchedule",
//                   "path": "gradeSchedule",
//                   "hidden": false,
//                   "component": "classroom/list",
//                   "meta": {
//                       "title": "年级课表",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "ClassroomSchedule",
//                   "path": "classroomSchedule",
//                   "hidden": false,
//                   "component": "classroom/list",
//                   "meta": {
//                       "title": "教室课表",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               }
//           ]
//       },
//       {
//           "name": "SemesterManage",
//           "path": "/semesterManage",
//           "hidden": false,
//           "redirect": "noRedirect",
//           "component": "Layout",
//           "alwaysShow": true,
//           "meta": {
//               "title": "分班成绩管理",
//               "icon": "date-range",
//               "noCache": false,
//               "link": null
//           },
//           "children": [
//               {
//                   "name": "Score/preference",
//                   "path": "score/preference",
//                   "hidden": false,
//                   "component": "score/preference/index",
//                   "meta": {
//                       "title": "显示偏好设置",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "/score/studentScore",
//                   "path": "/score/studentScore",
//                   "hidden": false,
//                   "component": "score/studentScore/index",
//                   "meta": {
//                       "title": "学生成绩",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "/score/manageClass",
//                   "path": "/score/manageClass",
//                   "hidden": false,
//                   "component": "score/manageClass/index",
//                   "meta": {
//                       "title": "班级教学分析—行政班",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "/score/teachingClass",
//                   "path": "/score/teachingClass",
//                   "hidden": false,
//                   "component": "score/teachingClass/index",
//                   "meta": {
//                       "title": "班级教学分析—教学班",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "ReportCard",
//                   "path": "reportCard",
//                   "hidden": false,
//                   "component": "score/reportCard/index",
//                   "meta": {
//                       "title": "学生成绩单",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               }
//           ]
//       },
//       {
//           "name": "DictManage",
//           "path": "/dictManage",
//           "hidden": false,
//           "redirect": "noRedirect",
//           "component": "Layout",
//           "alwaysShow": true,
//           "meta": {
//               "title": "字典管理",
//               "icon": "skill",
//               "noCache": false,
//               "link": null
//           },
//           "children": [
//               {
//                   "name": "AcademicYearSemesterManagement",
//                   "path": "academicYearSemesterManagement",
//                   "hidden": false,
//                   "component": "dictManage/academicYearSemesterManagement",
//                   "meta": {
//                       "title": "学年学期管理",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "GlobalDictionaryDefinition",
//                   "path": "globalDictionaryDefinition",
//                   "hidden": false,
//                   "component": "dictManage/globalDictionaryDefinition",
//                   "meta": {
//                       "title": "全局字典定义",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "UserDictionaryDefinition",
//                   "path": "userDictionaryDefinition",
//                   "hidden": false,
//                   "component": "dictManage/userDictionaryDefinition",
//                   "meta": {
//                       "title": "用户字典定义",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               }
//           ]
//       },
//       {
//           "name": "System",
//           "path": "/system",
//           "hidden": false,
//           "redirect": "noRedirect",
//           "component": "Layout",
//           "alwaysShow": true,
//           "meta": {
//               "title": "系统管理",
//               "icon": "system",
//               "noCache": false,
//               "link": null
//           },
//           "children": [
//               {
//                   "name": "User",
//                   "path": "user",
//                   "hidden": false,
//                   "component": "system/user/index",
//                   "meta": {
//                       "title": "用户管理",
//                       "icon": "user",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "Role",
//                   "path": "role",
//                   "hidden": false,
//                   "component": "system/role/index",
//                   "meta": {
//                       "title": "角色管理",
//                       "icon": "peoples",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "Menu",
//                   "path": "menu",
//                   "hidden": false,
//                   "component": "system/menu/index",
//                   "meta": {
//                       "title": "菜单管理",
//                       "icon": "tree-table",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "OverviewTeachingData",
//                   "path": "overviewTeachingData",
//                   "hidden": false,
//                   "component": "system/overviewTeachingData",
//                   "meta": {
//                       "title": "教学数据概览",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "Dept",
//                   "path": "dept",
//                   "hidden": true,
//                   "component": "system/dept/index",
//                   "meta": {
//                       "title": "部门管理",
//                       "icon": "tree",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "Post",
//                   "path": "post",
//                   "hidden": true,
//                   "component": "system/post/index",
//                   "meta": {
//                       "title": "岗位管理",
//                       "icon": "post",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "Dict",
//                   "path": "dict",
//                   "hidden": true,
//                   "component": "system/dict/index",
//                   "meta": {
//                       "title": "字典管理",
//                       "icon": "dict",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "Config",
//                   "path": "config",
//                   "hidden": true,
//                   "component": "system/config/index",
//                   "meta": {
//                       "title": "参数设置",
//                       "icon": "edit",
//                       "noCache": false,
//                       "link": null
//                   }
//               }
//           ]
//       },
//       {
//           "name": "Log",
//           "path": "/log",
//           "hidden": false,
//           "redirect": "noRedirect",
//           "component": "Layout",
//           "alwaysShow": true,
//           "meta": {
//               "title": "日志管理",
//               "icon": "checkbox",
//               "noCache": false,
//               "link": null
//           },
//           "children": [
//               {
//                   "name": "Logininfor",
//                   "path": "logininfor",
//                   "hidden": false,
//                   "component": "monitor/logininfor",
//                   "meta": {
//                       "title": "系统运行日志",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "Operlog",
//                   "path": "operlog",
//                   "hidden": false,
//                   "component": "monitor/operlog",
//                   "meta": {
//                       "title": "用户操作日志",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "AccessCircuitBreakerManagement",
//                   "path": "accessCircuitBreakerManagement",
//                   "hidden": false,
//                   "component": "monitor/online",
//                   "meta": {
//                       "title": "访问熔断管理",
//                       "icon": "#",
//                       "noCache": false,
//                       "link": null
//                   }
//               }
//           ]
//       },
//       {
//           "name": "Tool",
//           "path": "/tool",
//           "hidden": false,
//           "redirect": "noRedirect",
//           "component": "Layout",
//           "alwaysShow": true,
//           "meta": {
//               "title": "系统工具",
//               "icon": "tool",
//               "noCache": false,
//               "link": null
//           },
//           "children": [
//               {
//                   "name": "Build",
//                   "path": "build",
//                   "hidden": false,
//                   "component": "tool/build/index",
//                   "meta": {
//                       "title": "表单构建",
//                       "icon": "build",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "Gen",
//                   "path": "gen",
//                   "hidden": false,
//                   "component": "tool/gen/index",
//                   "meta": {
//                       "title": "代码生成",
//                       "icon": "code",
//                       "noCache": false,
//                       "link": null
//                   }
//               },
//               {
//                   "name": "Swagger",
//                   "path": "swagger",
//                   "hidden": false,
//                   "component": "tool/swagger/index",
//                   "meta": {
//                       "title": "系统接口",
//                       "icon": "swagger",
//                       "noCache": false,
//                       "link": null
//                   }
//               }
//           ]
//       }
//   ];
//   resolve({data: data})
// })
}