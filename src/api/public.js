// 公共接口
import request from '@/utils/request';

/**
 * 所有班级查询
 */
export const getGreadAll = (data) => {
  return request({
    url: 'system/classinfo/getGreadAll',
    method: 'post',
    data
  });
};

export const classinfoExport = (data) => {
  return request({
    url: 'system/classinfo/export',
    method: 'post',
    responseType: 'blob',
    data
  });
};
export const courseinfoExport = (data) => {
  return request({
    url: 'system/courseinfo/export',
    method: 'post',
    responseType: 'blob',
    data
  });
};
// /
export const studentsExport = (data) => {
  return request({
    url: 'system/students/export',
    method: 'post',
    responseType: 'blob',
    data
  });
};

export const teachersExport = (data) => {
  return request({
    url: 'system/teachers/export',
    method: 'post',
    responseType: 'blob',
    data
  });
};

export const teachtasktExport = (data) => {
  return request({
    url: 'system/teachtaskt/export',
    method: 'post',
    responseType: 'blob',
    data
  });
};

// /system/teachtaskt/export
