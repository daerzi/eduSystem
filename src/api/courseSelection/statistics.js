import request from "@/utils/request";

// 年级汇总
export const gradeSummary = (data) => {
  return request({
    url: "system/courseselect/gradeSummary",
    method: "post",
    data
  });
};

// 科目汇总
export const getCoseList = (data) => {
  return request({
    url: "system/courseselect/getCoseList",
    method: "post",
    data
  });
};
// 课组汇总
export const getCoseListGroup = (data) => {
  return request({
    url: "system/courseselect/getCoseListGroup",
    method: "post",
    data
  });
};
// 学生信息
export const getStudentList = (data) => {
  return request({
    url: "system/courseselect/getStudentList",
    method: "post",
    data
  });
};