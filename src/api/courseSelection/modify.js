import request, { download } from "@/utils/request";

// 学生选课结果 modify

// 列表查询
export const modifyList = (data) => {
  return request({
    url: "system/courseselect/list",
    method: "post",
    data,
  });
};

// 新增选课
export const modifyAdd = (data) => {
  return request({
    url: "system/courseselect/add",
    method: "post",
    data,
  });
};

// 详情查询
export const modifyInfo = (data) => {
  return request({
    url: `system/courseselect/getInfo/${data}`,
    method: "get",
  });
};
// 学生选课详情查询
export const getUserInfo = (data) => {
  return request({
    url: `system/courseselect/getUserInfo/${data}`,
    method: "get",
  });
};
// 删除
export const modifyRemove = (data) => {
  return request({
    url: "system/courseselect/remove",
    method: "post",
    data,
  });
};

// 编辑选课
export const modifyEdit = (data) => {
  return request({
    url: "system/courseselect/edit",
    method: "post",
    data,
  });
};
//  学生信息
export const getStudent = (userId) => {
  return request({
    url: "system/students/getStudent/" + userId,
    method: "get",
  });
};

//  获取最新选课信息
export const getCoursSelectInfo = (data) => {
  return request({
    url: "system/courseselectsetup/getCoursSelectInfo",
    method: "post",
    data: data
  });
};

// 选课科目列表
export const getActiveModelInfo = (id) => {
  return request({
    url: "system/courseselectmode/getActiveModelInfo/" + id,
    method: "get",
  });
};