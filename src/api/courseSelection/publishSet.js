import request, { download } from "@/utils/request";

// 选课设置 publishSet

// 禁止选课学生列表查询
export const publishSetList = (data) => {
  return request({
    url: "system/courseselectban/list",
    method: "post",
    data,
  });
};

// 添加禁止选课的学生
export const publishSetAdd = (data) => {
  return request({
    url: "system/courseselectban/add",
    method: "post",
    data,
  });
};

// 删除禁止选课的学生
export const publishSetRemove = (data) => {
  return request({
    url: "system/courseselectban/remove",
    method: "post",
    data,
  });
};

// 新增选课信息
export const publishSetInfoAdd = (data) => {
  return request({
    url: "system/courseselectsetup/add",
    method: "post",
    data,
  });
};

export const modifyGetActiveModel = (data) => {
  return request({
    url: `system/courseselectmode/getActiveModel`,
    method: "get",
  });
};
