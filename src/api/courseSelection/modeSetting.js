import request, { download } from "@/utils/request";

// 选课管理

// 选课模式列表查询
export const modeSettingList = (data) => {
  return request({
    url: "system/courseselectmode/list",
    method: "post",
    data,
  });
};

// 选课模式详情查询
export const modeSettingGetInfo = (data) => {
  return request({
    url: `system/courseselectmode/getInfo/${data}`,
    method: "get",
  });
};

// 新增选课模式
export const modeSettingAdd = (data) => {
  return request({
    url: "system/courseselectmode/add",
    method: "post",
    data,
  });
};

// 删除选课模式
export const modeSettingRemove = (data) => {
  return request({
    url: "system/courseselectmode/remove",
    method: "post",
    data,
  });
};

// 激活选课模式
export const modeSettingActivation = (data) => {
  return request({
    url: "system/courseselectmode/activation",
    method: "post",
    data,
  });
};

// 课程列表查询
export const getCourseList = (data) => {
  return request({
      url: 'system/courseinfo/list',
      method: 'post',
      data: data
    })
}