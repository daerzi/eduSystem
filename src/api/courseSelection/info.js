import request, { download } from "@/utils/request";

// 选课设置 info

// 选课信息列表查询
export const infoList = (data) => {
  return request({
    url: "system/courseselectsetup/list",
    method: "post",
    data,
  });
};

// 新增选课信息
export const infoAdd = (data) => {
  return request({
    url: "system/courseselectsetup/add",
    method: "post",
    data,
  });
};

// 查询详情
export const infoGetInfo = (data) => {
  return request({
    url: `system/courseselectsetup/getInfo/${data}`,
    method: "get",
  });
};

// 删除
export const infoRemove = (data) => {
  return request({
    url: "system/courseselectsetup/remove",
    method: "post",
    data,
  });
};

// 发布
export const infoPush = (data) => {
  return request({
    url: `system/courseselectsetup/push/${data}`,
    method: "get",
  });
};

// 延期
export const infoExtensio = (data) => {
  return request({
    url: "system/courseselectsetup/extension",
    method: "post",
    data,
  });
};
