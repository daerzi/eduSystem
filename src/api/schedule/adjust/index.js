import request from '@/utils/request'

// 是否可进行调课
export function exchange (data) {
  return request({
    url: '/system/switch/exchange',
    method: 'post',
    data
  })
}
// 调课位置交换
export function compile (data) {
  return request({
    url: '/system/switch/compile',
    method: 'post',
    data
  })
}
