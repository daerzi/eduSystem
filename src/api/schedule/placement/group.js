import request from '@/utils/request'

// 执行分组
export function subGroup (data) {
  return request({
    url: '/system/divide/subGroup',
    method: 'post',
    data
  })
}

// 执行分组
export function queryGroup (data) {
  return request({
    url: '/system/divide/queryGroup',
    method: 'post',
    data
  })
}
export function queryGroups (data) {
  return request({
    url: '/system/divide/queryGroups',
    method: 'post',
    data
  })
}
// 执行分组
export function saveGroup (data) {
  return request({
    url: '/system/divide/saveGroup',
    method: 'post',
    data
  })
}

// 执行分组
export function save (data) {
  return request({
    url: '/system/divide/save',
    method: 'post',
    data
  })
}