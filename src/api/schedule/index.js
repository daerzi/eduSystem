import request from '@/utils/request'

// 获取详情
export function getScheduleDetail(id) {
  return request({
    url: '/schedule/' + id,
    method: 'get'
  })
}
// 课表管理学生课表接口查询
export function getStudentCurriculum(data) {
  return request({
    url: 'system/students/list',
    method: 'post',
    data: data
  })
}

// 列表查询数据
export function getSectiontimeList(data) {
  return request({
    url: '/system/sectiontime/list',
    method: 'post',
    data
  })
}

// 新增节次
export function sectionAdd(data) {
  return request({
    url: '/system/sectiontime/add',
    method: 'post',
    data
  })
}

// 删除节次
export function sectionRemove(data) {
  return request({
    url: '/system/sectiontime/remove',
    method: 'post',
    data
  })
}

// 加课列表查询
export function getAddcoursedayList(data){
  return request({
    url: '/system/sectiontime/getAaddcoursedayList',
    method: 'post',
    data
  })
}

// 加课下拉框查询
export function getAddcoursedaySelect(data){
  return request({
    url: '/system/sectiontime/getAaddcoursedaySelect',
    method: 'post',
    data
  })
}

// 新增加课
export function addAddcourseday(data){
  return request({
    url: '/system/sectiontime/addAaddcourseday',
    method: 'post',
    data
  })
}

// 删除加课
export function deleteAddcourseday(data){
  return request({
    url: '/system/sectiontime/deleteAaddcourseday',
    method: 'post',
    data
  })
}