import request from '@/utils/request'

// 获取数量列表
export function getDivideListCourse (data) {
  return request({
    url: '/system/divide/listCourse',
    method: 'post',
    data
  })
}
