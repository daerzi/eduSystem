import request from '@/utils/request'

// 教师课程有效性检查
export function taxkTeacherCheck (data) {
  return request({
    url: '/system/absence/taxkTeacherCheck',
    method: 'post',
    data
  })
}
// 学生课程有效性检查
export function taxkStudentCheck (data) {
  return request({
    url: '/system/absence/taxkStudentCheck',
    method: 'post',
    data
  })
}
