import request from '@/utils/request'

// 获取数量列表
export function getDivideList (data) {
  return request({
    url: '/system/divide/list',
    method: 'post',
    data
  })
}
