import request, { download } from '@/utils/request';

// 走班管理

/**
 * 获取班级列表
 */
export const classinfoList = (data) => {
  return request(
    {
      url: 'system/classinfo/list',
      method: 'post',
      data: data
    },
    {
      timeout: data.pageSize == '10000000' ? 1000000 : 60000
    }
  );
};

/**
 * 新增班
 */
export const classinfoAdd = (data) => {
  return request({
    url: 'system/classinfo/add',
    method: 'post',
    data: data
  });
};

/**
 * 删除班
 */
export const classinfoRemove = (data) => {
  return request({
    url: 'system/classinfo/remove',
    method: 'post',
    data: data
  });
};

/**
 * 修改行政班
 */
export const classinfoEdit = (data) => {
  return request({
    url: 'system/classinfo/edit',
    method: 'post',
    data: data
  });
};

/**
 * 获取行政班
 */
export const getManageClassDetail = (id, data) => {
  return request({
    url: 'system/classroominfo/getInfo/' + id,
    method: 'get',
    data: data
  });
};

/**
 * 导出班级
 */
export const exportManageClassroom = (queryParams) => {
  console.log(queryParams);
  download(
    'system/classinfo/export',
    queryParams,
    `classroom_${new Date().getTime()}.xlsx`
  );
};

/**
 * 查询行政班学生数据
 */
export const getManageStudentList = (data) => {
  return request({
    url: 'system/classinfo/getStudentList',
    method: 'post',
    data: data
  });
};
export const getTeacherAllByGread = (data) => {
  return request({
    url: 'system/teachers/getTeacherAllByGread',
    method: 'post',
    data: data
  });
};
// 教学班
export const getClassInfoTimetableinfo = (data, id = 39) => {
  return request({
    url: 'system/classinfo/getClassInfoTimetableinfo',
    method: 'post',
    data
  });
};
/**
 * 查询行政班老师
 */
export const classinfoGetTeacherInfo = (data) => {
  return request({
    url: 'system/classinfo/getTeacherInfo',
    method: 'post',
    data: data
  });
};

/**
 * 查询行政班课表
 */

export const getClassTimetableinfo = (data) => {
  return request({
    url: 'system/classinfo/getClassTimetableinfo',
    method: 'post',
    data: data
  });
};

/**
 * 获取教学班列表
 */
export const teachtaskList = (data) => {
  return request({
    url: 'system/teachtask/list',
    method: 'post',
    data: data
  });
};

/**
 * 新增教学班
 */
export const teachtaskAdd = (data) => {
  return request({
    url: 'system/teachtask/add',
    method: 'post',
    data: data
  });
};

/**
 * 删除教学班
 */
export const teachtaskRemove = (data) => {
  return request({
    url: 'system/teachtask/remove',
    method: 'post',
    data: data
  });
};

/**
 * 修改教学班
 */
export const teachtaskEdit = (data) => {
  return request({
    url: 'system/teachtask/edit',
    method: 'post',
    data: data
  });
};

/**
 * 获取教学班详情
 */
export const getTeachClassDetail = (id, data) => {
  return request({
    url: 'system/teachtask/getInfo/' + id,
    method: 'get',
    data: data
  });
};

/**
 * 导出教学班
 */
export const exportTeachClass = (queryParams) => {
  download(
    'system/teachtask/export',
    {
      ...queryParams
    },
    `teachclass_${new Date().getTime()}.xlsx`
  );
};

/**
 * 获取教学班学生数据
 */
export const getTeachStudentList = (data) => {
  return request({
    url: 'system/teachtask/getStudentList',
    method: 'post',
    data: data
  });
};

/**
 * 获取教学班老师
 */
export const getTeachTeacherInfo = (data) => {
  return request({
    url: 'system/teachtask/getTeacherInfo',
    method: 'post',
    data: data
  });
};

/**
 * 获取教学班课表
 */
export const getTeachClassSchedule = (data) => {
  return request({
    url: 'system/teachtask/getClassTimetableinfo',
    method: 'post',
    data: data
  });
};

// system/classinfo/getStudentList
export const getStudentListExport = (data) => {
  return request({
    url: 'system/classinfo/getStudentListExport',
    method: 'post',
    data: data
  });
};
