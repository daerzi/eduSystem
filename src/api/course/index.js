import request,{download} from '@/utils/request'

// 课程管理

// 新增课程
export const courseAdd = (data) => {
    return request({
        url: 'system/courseinfo/add',
        method: 'post',
        data: data
      })
}

// 编辑课程
export const courseEdit = (data) => {
  return request({
      url: 'system/courseinfo/edit',
      method: 'post',
      data: data
    })
}

// 查询详情
export const courseGetInfo = (data) => {
  return request({
      url: `system/courseinfo/getInfo/${data}`,
      method: 'get',
    })
}

// 列表查询
export const courseList = (data) => {
  return request({
      url: 'system/courseinfo/list',
      method: 'post',
      data: data
    })
}

// 删除
export const courseRemove = (data) => {
  return request({
      url: 'system/courseinfo/remove',
      method: 'post',
      data: data
    })
}

// 模板下载
export const courseExportTemplate = (data) => {
  return request({
      url: 'system/courseinfo/exportTemplate',
      method: 'post',
      data: data
    })
}

// 导出
export const courseExport = (data) => {
  return request({
      url: 'system/courseinfo/export',
      method: 'post',
      data: data
    })
}

// 导入
export const courseImport = (data) => {
  return request({
      url: 'system/courseinfo/import',
      method: 'post',
      data: data
    })
}