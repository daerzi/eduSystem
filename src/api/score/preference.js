import request, { download } from "@/utils/request";

// 显示偏好设置

// 列表查询
export const preferenceList = (data) => {
  return request({
    url: "system/displaypreferences/list",
    method: "post",
    data: data,
  });
};

// 新增
export const preferenceAdd = (data) => {
  return request({
    url: "system/displaypreferences/add",
    method: "post",
    data: data,
  });
};
// 详情
export const preferenceGetInfo = (id) => {
  return request({
    url: "system/displaypreferences/getInfo/" + id,
    method: "get",
  });
};
// 编辑
export const preferenceEdit = (data) => {
  return request({
    url: "system/displaypreferences/edit",
    method: "post",
    data: data,
  });
};

// 删除
export const preferenceEremovedit = (data) => {
  return request({
    url: "system/displaypreferences/remove",
    method: "post",
    data: data,
  });
};