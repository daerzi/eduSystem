import request, { download } from "@/utils/request";

// 学生成绩

// 获取动态表头
export const studentScoreGetAchievementHead = (data) => {
  return request({
    url: `system/schoolreport/getAchievementHead`,
    method: "post",
    data
  });
};

// 获取学生成绩
export const studentScoreGetAchievementList = (data) => {
  return request({
    url: `system/schoolreport/getAchievementList`,
    method: "post",
    data
  });
};
