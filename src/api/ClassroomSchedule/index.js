import request,{download} from '@/utils/request'


export const getClassScheduleList = (data)=>{
  return request({
    url:'system/classinfo/getClassInfoTimetableinfo',
    method:'post',
    data:data
  })
}
// 获取楼层
export const getFloor = ()=>{
  return request({
    url:'system/classroominfo/getfloor',
    method:'get'
  })
}
//获取楼宇
export const getBuilding = ()=>{
  return request({
    url:'system/classroominfo/getBuilding',
    method:'get',

  })
}
