import request,{download} from '@/utils/request'

// 课程任务教师
/**
 * 获取教师列表
 */
export const getTeachingTaskTeacherList = (data) => {
    return request({
        url: 'system/teachers/list',
        method: 'post',
        data: data
      })
}

export function getListByGradeAndYearAndTerm(params) {
  return request({
    url: 'system/classinfo/getListByGradeAndYearAndTerm/',
    method: 'get',
    params: params
  })
}

export function getListByGrade(gread) {
  return request({
    url: 'system/classinfo/getListByGrade/' + gread,
    method: 'get'
  })
}
export function studentsRemove(data) {
  return request({
    url: 'system/students/remove',
    method: 'post',
    data: data
  })
}


//列表查询
export function getStudentsList(data) {
  return request({
    url: 'system/students/list',
    method: 'post',
    data: data
  })
}

//获取获取学校信息
export function getBaseSchoolInfo() {
  return request({
    url: 'system/schoolinfo/getBaseSchoolInfo',
    method: 'get',
  })
}

export function getStudentInfo(data) {
  return request({
    url: 'system/students/getInfo/'+data,
    method: 'get',
  })
}



//获取学生学籍号
export function getStudentNumber(data) {
  return request({
    url: 'system/students/getStudentNumber',
    method: 'post',
    data: data
  })
}

//获取班级详情
export function getInfoByNumber(data) {
  return request({
    url: 'system/classinfo/getInfoByNumber/'+data,
    method: 'get',
  })
}

export function studentsInsert(data) {
  return request({
    url: 'system/students/add',
    method: 'post',
    data: data
  })
}

export function studentsEdit(data) {
  return request({
    url: 'system/students/edit',
    method: 'post',
    data: data
  })
}
