import request, { download } from '@/utils/request';
// 获取自习课类型
export const getselfclass = (data) => {
  return request({
    url: 'system/sectiontime/listSelf',
    method: 'post',
    data: data
  });
};
// 课程任务教师
/**
 * 获取教师列表
 */
export const getTeachingTaskTeacherList = (data) => {
  return request({
    url: 'system/teachers/list',
    method: 'post',
    data: data
  });
};
// 获取教学班列表
export const getTeachinggradeTeacherList = (data) => {
  return request({
    url: 'system/sectiontime/getClassList',
    method: 'post',
    data: data
  });
};
//获取排自习课的数据检查下一步
export const getselfchecklist = (data) => {
  return request({
    url: '/system/sectiontime/getSelfClassList',
    method: 'post',
    data: data
  });
};
//第五个页面(数据检验)下一步
export const getchecklist = (data) => {
  return request({
    url: '/system/sectiontime/courseSchedul',
    method: 'post',
    data: data
  });
};
export function getListByGrade(gread) {
  return request({
    url: 'system/classinfo/getListByGrade' + gread,
    method: 'get'
  });
}
export function teachtaskList(data) {
  return request({
    url: 'system/teachtask/list',
    method: 'post',
    data
  });
}

export function teachtaskAdd(data) {
  return request({
    url: 'system/teachtask/edit',
    method: 'post',
    data
  });
}

export function getListEdit(data) {
  return request({
    url: 'system/classinfo/edit',
    method: 'post',
    data
  });
}

export const addTeacher = (data) => {
  return request({
    url: 'system/teachers/add',
    method: 'post',
    data: data
  });
};

export const editTeacher = (data) => {
  return request({
    url: 'system/teachers/edit',
    method: 'post',
    data: data
  });
};

export const removeTeacher = (data) => {
  return request({
    url: 'system/teachers/remove',
    method: 'post',
    data: data
  });
};

export function getBaseInfo(gread) {
  return request({
    url: 'system/teachers/getInfo/' + gread,
    method: 'get'
  });
}
