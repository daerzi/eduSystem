import request, { download } from "@/utils/request";

// 学生异动管理  studentStatusChange

// 列表查询
export const studentStatusChangeList = (data) => {
  return request({
    url: "system/students/list",
    method: "post",
    data,
  });
};

// 添加异动信息
export const studentStatusChangeAdd = (data) => {
  return request({
    url: "system/studentstatus",
    method: "post",
    data,
  });
};