import request, { download } from "@/utils/request";

// 教师任务管理  teachTask

// 列表查询
export const teachTaskList = (data) => {
  return request({
    url: "system/teachtasktClass/list",
    method: "post",
    data,
  });
};

// 新增
export const teachTaskAdd = (data) => {
  return request({
    url: "system/teachtasktClass/add",
    method: "post",
    data,
  });
};

// 编辑
export const teachTaskEdit = (data) => {
  return request({
    url: "system/teachtasktClass/edit",
    method: "post",
    data,
  });
};

// 删除
export const teachTaskRemove = (data) => {
  return request({
    url: "system/teachtasktClass/remove",
    method: "post",
    data,
  });
};

// 判断能否修改
export const teachTaskCheckCanChange = (data) => {
  return request({
    url: "system/teachtasktClass/checkCanChange",
    method: "post",
    data,
  });
};
