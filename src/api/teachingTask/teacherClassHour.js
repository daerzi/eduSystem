import request, { download } from "@/utils/request";

// 教师课时量统计

// 列表
export const teacherClassHourList = (data) => {
  return request({
    url: "system/teachers/listCourseLoad",
    method: "post",
    data,
  });
};
