import request,{download} from '@/utils/request'

// 课程任务教师
/**
 * 获取教师列表
 */
export const getCoursListAll = (data) => {
    return request({
        url: 'system/courseinfo/listAll',
        method: 'post',
        data: data
      })
}

