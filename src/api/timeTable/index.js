import request, { download } from "@/utils/request";

// 课表管理

/**
 * 获取学生课表
 */
export const getStudentSectiontime = (params) => {
  return request({
    url: "system/students/getStudentSectiontime/" + params.id,
    method: "get",
    params: { type: params.type },
  });
};
// 获取老师课程
export const getTeacherSectiontime = (params) => {
  return request({
    url: "system/teachers/getTeacherSectiontime/" + params.id,
    method: "get",
    params: { type: params.type },
  });
};

// 获取行政班课表
export const getClassTimetableinfo = (params) => {
  return request({
    url: "/system/classinfo/getClassTimetableinfo",
    method: "post",
    data: params,
  });
};

// 教学班课表
export const getClassInfoTimetableinfo = (params) => {
  return request({
    url: "/system/classinfo/getClassInfoTimetableinfo",
    method: "post",
    data: params,
  });
};

// 教室课表
export const getClassDoTimetableinfo = (params) => {
  return request({
    url: "/system/classinfo/getClassDoTimetableinfo",
    method: "post",
    data: params,
  });
};

// 个人课表查询
export const getUserSectiontime = (params) => {
  return request({
    url: "/system/sectiontime/getUserSectiontime/" + params.userId,
    method: "get",
  });
};

/**
 * 获取学生课表节次
 */
export const getSectiontimeBase = (data) => {
  return request({
    url: "system/sectiontime/sectiontimeBase",
    method: "post",
    data,
  });
};

/**
 * 获取排课表
 */
export const getSectiontimeCourseSchedul = (params) => {
  return request({
    url: "system/sectiontime/courseSchedul",
    method: "post",
    data: params,
  });
};

/**
 * 获取年级课表
 */
export const queryGradeSchedule = (params) => {
  return request({
    url: "system/sectiontime/gradeSchedule",
    method: "post",
    data: params,
  });
};
