import request,{download} from '@/utils/request'

// 考勤管理模块的相关接口


/**
 *  子模块：考勤查询
 */
// 获取考勤查询的列表
export const getAttendanceQueryList = (data) => {
  return request({
    url: 'system/clocking/list',
    method: 'post',
    data
  })
}
// 按条件筛选考勤查询的列表
export const getAttendanceQueryListOfSearch = (data) => {
  return request({
    url: 'system/clocking/list',
    method: 'post',
    data
  })
}
// 导出
export const exportAttendanceQueryList = (data) => {
  return request({
    url: 'system/clocking/export',
    method: 'post',
    data
  })
}


/**
 *  子模块：缺勤统计
 */
// 获取缺勤统计的列表
export const getAbsenceStatisticList = (data) => {
  return request({
    url: 'system/absence/list',
    method: 'post',
    data
  })
}
// 按条件筛选缺勤统计的列表
export const getAbsenceStatisticListOfSearch = (data) => {
  return request({
    url: 'system/absence/list',
    method: 'post',
    data
  })
}
// 导出
export const exportAbsenceStatisticList = (data) => {
  return request({
    url: 'system/absence/export',
    method: 'post',
    data
  })
}


/**
 *  子模块：考勤补点修改
 */
// 获取考勤补点修改的列表
export const getEditAttendanceList = (data) => {
  return request({
    url: 'system/reissue/list',
    method: 'post',
    data
  })
}
// 按条件筛选考勤补点修改的列表
export const getEditAttendanceListOfSearch = (data) => {
  return request({
    url: 'system/reissue/list',
    method: 'post',
    data
  })
}
// 导出
export const exportEditAttendanceList = (data) => {
  return request({
    url: 'system/reissue/export',
    method: 'post',
    data
  })
}


/**
 *  子模块：教学班点名查询
 */
// 获取教学班点名查询的列表
export const getRollCallSearchList = (data) => {
  return request({
    url: 'system/rollCallInquire/list',
    method: 'post',
    data
  })
}
// 按条件筛选教学班点名查询的列表
export const getRollCallSearchListOfSearch = (data) => {
  return request({
    url: 'system/rollCallInquire/list',
    method: 'post',
    data
  })
}
// 导出
export const exportRollCallSearchList = (data) => {
  return request({
    url: 'system/rollCallInquire/export',
    method: 'post',
    data
  })
}


/**
 *  子模块：教学班点名
 */
// 获取教学班点名的列表
export const getRollCallList = (data) => {
  return request({
    url: 'system/rollCall/list',
    method: 'post',
    data
  })
}
// 获取教学班点名的详情列表
export const getRollCallListOfDetail = (data) => {
  return request({
    url: 'system/rollCall/detail/list',
    method: 'post',
    data
  })
}
// 教学班点名的提交
export const addRollCallData = (data) => {
  return request({
    url: 'system/rollCall/insert',
    method: 'post',
    data
  })
}