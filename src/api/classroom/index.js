import request, { download } from '@/utils/request';
/**
 * 获取教室列表
 */
export const getClassroomList = (data) => {
  return request({
    url: 'system/classroominfo/list',
    method: 'post',
    data: data
  });
};

export const getClassDoTimetableinfo = (data) => {
  return request({
    url: 'system/classinfo/getClassDoTimetableinfo',
    method: 'post',
    data: data
  });
};

/**
 * 新增教室
 */
export const addClassroom = (data) => {
  return request({
    url: 'system/classroominfo/add',
    method: 'post',
    data: data
  });
};

/**
 * 删除教室
 */
export const deleteClassroom = (data) => {
  return request({
    url: 'system/classroominfo/remove',
    method: 'post',
    data: data
  });
};

/**
 * 修改教室
 */
export const updateClassroom = (data) => {
  return request({
    url: 'system/classroominfo/edit',
    method: 'post',
    data: data
  });
};

/**
 * 获取教室详情
 */
export const getClassroomDetail = (id, data) => {
  return request({
    url: 'system/classroominfo/getInfo/' + id,
    method: 'get',
    data: data
  });
};

/**
 * 下载教室导入模板
 */
export const downloadClassroomTemplate = (queryParams) => {
  download(
    'system/classroominfo/exportTemplate',
    {
      ...queryParams
    },
    `classroom_${new Date().getTime()}.xlsx`
  );
};

/**
 * 导入教室
 */
export const importClassroom = (data) => {
  return request({
    url: 'system/classroominfo/import',
    method: 'post',
    data: data
  });
};

/**
 * 导出教室
 */
export const exportClassroom = (queryParams) => {
  return request({
    url: 'system/classroominfo/export',
    method: 'post',
    data: queryParams,
    responseType: 'blob'
  });
  // download('system/classroominfo/export', {
  //   ...queryParams
  //   }, `classroom_${new Date().getTime()}.xlsx`)
};

/**
 * 获取教室编号
 */
export const getClassroomCode = () => {
  return request({
    url: 'system/classroominfo/getCode',
    method: 'get'
  });
};

/**
 * 获取教室类型
 */
export const getClassroomType = () => {
  return request({
    url: 'system/classroominfo/getType',
    method: 'get'
  });
};

/**
 * 获取学年
 */
export const getSchoolYear = () => {
  return request({
    url: 'system/classroominfo/getSchoolYear',
    method: 'get'
  });
};

/**
 * 获取学期
 */
export const getSemester = () => {
  return request({
    url: 'system/classroominfo/getSemester',
    method: 'get'
  });
};

// 获取楼宇
export const getBuilding = () => {
  return request({
    url: 'system/classroominfo/getBuilding',
    method: 'get'
  });
};

// 获取楼层
export const getfloor = () => {
  return request({
    url: 'system/classroominfo/getfloor',
    method: 'get'
  });
};
