import Vue from 'vue';
import Vuex from 'vuex';
import app from './modules/app';
import dict from './modules/dict';
import user from './modules/user';
import tagsView from './modules/tagsView';
import permission from './modules/permission';
import settings from './modules/settings';
import getters from './getters';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    student: {}
  },
  mutations: {
    SET_STUDENT(state, data) {
      state.student = data;
    }
  },
  actions: {
    setStudent({ commit }, data) {
      commit('SET_STUDENT', data);
    }
  },
  modules: {
    app,
    dict,
    user,
    tagsView,
    permission,
    settings
  },
  getters
});

export default store;
