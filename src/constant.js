// 静态变量

// 星期
export const weekdays = ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日'];

// 分班步骤
export const scheduleSteps = ['信息选择', '数量设置', '班级调整', '执行分组', '方案确认', '保存结果'];

// 排课流程
export const scheduleProcess = ['基本信息', '课程节次设置', '教研会议设置', '教学班任课老师指定', '数据检验', '课表方案预览', '完成'];
// 排自习课课流程
export const selfStudyProcess = ['基本信息', '数据检验', '课表方案预览', '完成'];
// 校验步骤
export const checkSteps = ['检查类别', '检查设置', '检查中', '检查结果'];

// 性别字典
export const sexDict = [{ label: '男', value: '1' }, { label: '女', value: '2' }];

// 汉族	01
// 蒙古族	02
// 回族	03
// 藏族	04
// 维吾尔族	05 
// 苗族	06
// 彝族	07
// 壮族	08
// 布依族	09
// 朝鲜族	10 
// 满族	11
// 侗族	12
// 瑶族	13
// 白族	14
// 土家族	15
// 哈尼族	16
// 哈萨克族	17 
// 傣族	18
// 黎族	19
// 僳僳族	20
// 佤族	21
// 畲族	22 
// 高山族	23 
// 拉祜族	24
// 水族	25
// 东乡族	26
// 纳西族	27
// 景颇族	28
// 柯尔克孜族	29
// 土族	30
// 达斡尔族	31
// 仫佬族	32
// 羌族	33
// 布朗族	34
// 撒拉族	35
// 毛难族	36
// 仡佬族	37
// 锡伯族	38 
// 阿昌族	39
// 普米族	40
// 塔吉克族	41
// 怒族	42
// 乌孜别克族	43
// 俄罗斯族	44
// 鄂温克族	45
// 崩龙族	46
// 保安族	47
// 裕固族	48
// 京族	49
// 塔塔尔族	50
// 独龙族	51
// 鄂伦春族	52
// 赫哲族	53
// 门巴族	54
// 珞巴族	55
// 基诺族	56
// 其它	57
// 外国血统	58
// 民族字典
export const nationDict = [
  { label: '汉族', value: '01' },
  { label: '蒙古族', value: '02' },
  { label: '回族', value: '03' },
  { label: '藏族', value: '04' },
  { label: '维吾尔族', value: '05' },
  { label: '苗族', value: '06' },
  { label: '彝族', value: '07' },
  { label: '壮族', value: '08' },
  { label: '布依族', value: '09' },
  { label: '朝鲜族', value: '10' },
  { label: '满族', value: '11' },
  { label: '侗族', value: '12' },
  { label: '瑶族', value: '13' },
  { label: '白族', value: '14' },
  { label: '土家族', value: '15' },
  { label: '哈尼族', value: '16' },
  { label: '哈萨克族', value: '17' },
  { label: '傣族', value: '18' },
  { label: '黎族', value: '19' },
  { label: '僳僳族', value: '20' },
  { label: '佤族', value: '21' },
  { label: '畲族', value: '22' },
  { label: '高山族', value: '23' },
  { label: '拉祜族', value: '24' },
  { label: '水族', value: '25' },
  { label: '东乡族', value: '26' },
  { label: '纳西族', value: '27' },
  { label: '景颇族', value: '28' },
  { label: '柯尔克孜族', value: '29' },
  { label: '土族', value: '30' },
  { label: '达斡尔族', value: '31' },
  { label: '仫佬族', value: '32' },
  { label: '羌族', value: '33' },
  { label: '布朗族', value: '34' },
  { label: '撒拉族', value: '35' },
  { label: '毛难族', value: '36' },
  { label: '仡佬族', value: '37' },
  { label: '锡伯族', value: '38' },
  { label: '阿昌族', value: '39' },
  { label: '普米族', value: '40' },
  { label: '塔吉克族', value: '41' },
  { label: '怒族', value: '42' },
  { label: '乌孜别克族', value: '43' },
  { label: '俄罗斯族', value: '44' },
  { label: '鄂温克族', value: '45' },
  { label: '崩龙族', value: '46' },
  { label: '保安族', value: '47' },
  { label: '裕固族', value: '48' },
  { label: '京族', value: '49' },
  { label: '塔塔尔族', value: '50' },
  { label: '独龙族', value: '51' },
  { label: '鄂伦春族', value: '52' },
  { label: '赫哲族', value: '53' },
  { label: '门巴族', value: '54' },
  { label: '珞巴族', value: '55' },
  { label: '基诺族', value: '56' },
  { label: '其它', value: '57' },
  { label: '外国血统', value: '58' }
];

// 教师类型			
// 任课教师	01	
// 年级主任	02	
// 学科组长	03	
// 中层干部	04	
// 校领导	05	
// 管理人员	06	
// 其它	07	
// 教师类型字典
export const teacherTypeDict = [
  { label: '任课教师', value: '01' },
  { label: '年级主任', value: '02' },
  { label: '学科组长', value: '03' },
  { label: '中层干部', value: '04' },
  { label: '校领导', value: '05' },
  { label: '管理人员', value: '06' },
  { label: '其它', value: '07' }
];

// 学生类型
// 	普通在校生	01
// 	留学生	02
// 	交换生	03
// 	择校生	04
// 	其它	05
// 学生类型字典
export const studentTypeDict = [
  { label: '普通在校生', value: '01' },
  { label: '留学生', value: '02' },
  { label: '交换生', value: '03' },
  { label: '择校生', value: '04' },
  { label: '其它', value: '05' }
];

// 学籍异动类型		
// 	转入	01
// 	转出	02
// 	留级	03
// 	休学	04
// 	死亡	05
// 	其它	06
// 学籍异动类型字典
export const studentChangeTypeDict = [
  { label: '转入', value: '01' },
  { label: '转出', value: '02' },
  { label: '留级', value: '03' },
  { label: '休学', value: '04' },
  { label: '死亡', value: '05' },
  { label: '其它', value: '06' }
];

// 学生成绩类型		
// 	分班成绩	01
// 	一次月考	02
// 	二次月考	03
// 	三次月考	04
// 	四次月考	05
// 	期中考试	06
// 	期末考试	07
// 学生成绩类型字典
export const studentScoreTypeDict = [
  { label: '分班成绩', value: '01' },
  { label: '一次月考', value: '02' },
  { label: '二次月考', value: '03' },
  { label: '三次月考', value: '04' },
  { label: '四次月考', value: '05' },
  { label: '期中考试', value: '06' },
  { label: '期末考试', value: '07' }
];

// 教室类型		
// 	普通教室	01
// 	阶梯教室	02
// 	音乐教室	03
// 	美术教室	04
// 	直播教室	05
// 	劳动技能教室	06
// 	创新能力培养教室	07
// 	运动场	08
// 	体育馆	09
// 	游泳馆	10
// 	办公室	11
// 	实验室	12
// 教室类型字典
export const classroomTypeDict = [
  { label: '普通教室', value: '01' },
  { label: '阶梯教室', value: '02' },
  { label: '音乐教室', value: '03' },
  { label: '美术教室', value: '04' },
  { label: '直播教室', value: '05' },
  { label: '劳动技能教室', value: '06' },
  { label: '创新能力培养教室', value: '07' },
  { label: '运动场', value: '08' },
  { label: '体育馆', value: '09' },
  { label: '游泳馆', value: '10' },
  { label: '办公室', value: '11' },
  { label: '实验室', value: '12' }
];

// 学校类别		
// 	普通高中	01
// 	完全中学	02
// 	普通初中	03
// 	九年一贯制学校	04
// 	十二年一贯制学校	05
// 	小学	06
// 	幼儿园	07
// 	中专	08
// 	技校	09
// 	其它	10
// 学校类别字典
export const schoolTypeDict = [
  { label: '普通高中', value: '01' },
  { label: '完全中学', value: '02' },
  { label: '普通初中', value: '03' },
  { label: '九年一贯制学校', value: '04' },
  { label: '十二年一贯制学校', value: '05' },
  { label: '小学', value: '06' },
  { label: '幼儿园', value: '07' },
  { label: '中专', value: '08' },
  { label: '技校', value: '09' },
  { label: '其它', value: '10' }
];

// 所属学段		
// 	高中	01
// 	中专	02
// 	初中	03
// 	小学	04
// 	中专	05
// 	幼儿园	06
// 所属学段字典
export const schoolSectionDict = [
  { label: '高中', value: '01' },
  { label: '中专', value: '02' },
  { label: '初中', value: '03' },
  { label: '小学', value: '04' },
  { label: '中专', value: '05' },
  { label: '幼儿园', value: '06' }
];

// 课时类型	
// 	正课
// 	加课
// 课时类型字典
export const classHourTypeDict = [
  { label: '正课', value: '01' },
  { label: '加课', value: '02' }
];

// 课程类型	
// 	必修课
// 	选考课
// 	其它
// 课程类型字典
export const courseTypeDict = [
  { label: '必修课', value: '01' },
  { label: '选考课', value: '02' },
  { label: '其它', value: '03' }
];

// 是否高考科目	
// 	高考科目
// 	非高考科目
// 是否高考科目字典
export const isGaokaoDict = [
  { label: '高考科目', value: '01' },
  { label: '非高考科目', value: '02' }
];

// 考勤类型	
// 	正常
// 	旷课
// 	病假
// 	病假--乙类乙管及以上传染病
// 	事假
// 	公假
// 	迟到
// 	早退
// 	其它
// 考勤类型字典
export const attendanceTypeDict = [
  { label: '正常', value: '01' },
  { label: '旷课', value: '02' },
  { label: '病假', value: '03' },
  { label: '病假--乙类乙管及以上传染病', value: '04' },
  { label: '事假', value: '05' },
  { label: '公假', value: '06' },
  { label: '迟到', value: '07' },
  { label: '早退', value: '08' },
  { label: '其它', value: '09' }
];

// 班级类型	
// 	普通班
// 	实验班
// 	择校班
// 	创新实验班
// 	分类辅导班
// 	少数民族班
// 	特长班
// 班级类型字典
export const classTypeDict = [
  { label: '普通班', value: '01' },
  { label: '实验班', value: '02' },
  { label: '择校班', value: '03' },
  { label: '创新实验班', value: '04' },
  { label: '分类辅导班', value: '05' },
  { label: '少数民族班', value: '06' },
  { label: '特长班', value: '07' }
];